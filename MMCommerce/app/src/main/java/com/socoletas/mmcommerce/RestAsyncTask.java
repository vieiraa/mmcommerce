package com.socoletas.mmcommerce;

import android.accounts.Account;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.support.Base64;
import org.springframework.web.client.RestTemplate;

/**
 * Created by echamca on 12/7/2016.
 */

public class RestAsyncTask extends AsyncTask<Void, Void, Boolean> {
    public AsyncResponse delegate = null;

    private Login mLogin;
    private String mUrl;
    private Object mObject;

    public RestAsyncTask(Login login, String url, Object object)
    {
        mLogin = login;
        mUrl = url;
        mObject = object;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        try {

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);

            String authorization = null;
            if(mLogin != null )
            {
                authorization = mLogin.getUsername()+":"+mLogin.getPassword();
            }

            if(authorization != null)
            {
                byte[] plainCredsBytes = authorization.getBytes();
                byte[] base64CredsBytes = Base64.encodeBytesToBytes(plainCredsBytes);
                String base64Creds = new String(base64CredsBytes);
                requestHeaders.add("Authorization", "Basic " + base64Creds);
            }

            HttpEntity<Object> requestEntity = new HttpEntity<Object>(mObject, requestHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<String> response = restTemplate.exchange(mUrl,HttpMethod.POST,requestEntity, String.class);

            HttpStatus httpStatus = response.getStatusCode();
            if(HttpStatus.OK == httpStatus)
            {
                return true;
            }

            return false;

        } catch (Exception e) {
            Log.e("LoginActivity", e.getMessage(), e);
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        delegate.processFinish(result);

    }

}
