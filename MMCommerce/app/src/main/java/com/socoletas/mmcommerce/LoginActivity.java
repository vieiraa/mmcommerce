package com.socoletas.mmcommerce;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.socoletas.mmcommerce.qrcode.QRCodePhotoReader;
import com.socoletas.mmcommerce.qrcode.QRCodePwActivity;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements AsyncResponse {

    // UI references.
    private AutoCompleteTextView mUserView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Bundle bundle = new Bundle();
    private Intent intent;
    Login login = new Login();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ericssonbrand);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        super.onCreate(savedInstanceState);
        intent = new Intent(this, QRCodePhotoReader.class);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mUserView = (AutoCompleteTextView) findViewById(R.id.user);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    public void processFinish(Boolean output){
        if (!output) {
            String url = "https://mmcommerce.herokuapp.com/signup";
            RestAsyncTask restAsyncTask = (RestAsyncTask) new RestAsyncTask(null,url, login);
            restAsyncTask.delegate = this;
            restAsyncTask.execute();

//            mUserView.setError(getString(R.string.error_invalid_login));
//            View focusView = mUserView;
//            focusView.requestFocus();
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Login result");
//            builder.setMessage("Invalid login");
//            AlertDialog alert1 = builder.create();
//            alert1.show();
        }else
        {
            startActivity(intent);
        }
    }



     /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        String user = mUserView.getText().toString();
        String password = mPasswordView.getText().toString();

        login.setUsername(user);
        login.setPassword(password);

        bundle.putString("userName",login.getUsername());
        bundle.putString("password",login.getPassword());
        intent.putExtras(bundle);

        String url = "https://mmcommerce.herokuapp.com/login";
        RestAsyncTask restAsyncTask = (RestAsyncTask) new RestAsyncTask(login,url, null);
        restAsyncTask.delegate = this;
        restAsyncTask.execute();

        //String response = intent.getStringExtra(RestTask.HTTP_RESPONSE);



//        boolean cancel = false;
//        View focusView = null;
//
//        if (false) {
//            mUserView.setError(getString(R.string.error_invalid_login));
//            focusView = mUserView;
//            cancel = true;
//        }
//
//        // Check for a valid password, if the user entered one.
//        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
//            mPasswordView.setError(getString(R.string.error_invalid_password));
//            focusView = mPasswordView;
//            cancel = true;
//        }
//        if (cancel) {
//            // There was an error; don't attempt login and focus the first
//            // form field with an error.
//            focusView.requestFocus();
//        } else {
//            // Show a progress spinner, and kick off a background task to
//            // perform the user login attempt.
//            showProgress(true);
//
//        }
    }


}

