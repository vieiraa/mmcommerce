package com.socoletas.mmcommerce;

/**
 * Created by echamca on 12/8/2016.
 */

public interface AsyncResponse {
    void processFinish(Boolean output);
}
