package com.socoletas.mmcommerce.utils;

/**
 * Created by edazfre on 07/12/2016.
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class AssetsPropertyReader {
        private Context context;
        private Properties properties;

        public static final String GLOBAL_VALUE_PROPERTIES =  "GlobalValues.properties";
        public static final String WORKING_MODE_KEY = "WorkingMode";

        public AssetsPropertyReader(Context context) {
            this.context = context;
            /**
             * Constructs a new Properties object.
             */
            properties = new Properties();
        }

        public Properties getProperties(String FileName) {

            try {
                /**
                 * getAssets() Return an AssetManager instance for your
                 * application's package. AssetManager Provides access to an
                 * application's raw asset files;
                 */
                AssetManager assetManager = context.getAssets();
                /**
                 * Open an asset using ACCESS_STREAMING mode. This
                 */
                InputStream inputStream = assetManager.open(FileName);
                /**
                 * Loads properties from the specified InputStream,
                 */
                properties.load(inputStream);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.e("AssetsPropertyReader",e.toString());
            }
            return properties;

        }

}
