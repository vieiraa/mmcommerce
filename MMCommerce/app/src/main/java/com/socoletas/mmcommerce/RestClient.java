package com.socoletas.mmcommerce;

import android.accounts.Account;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.support.Base64;
import org.springframework.web.client.RestTemplate;

import static android.R.id.message;

/**
 * Created by echamca on 12/7/2016.
 */

public class RestClient{

        protected boolean executePost(Login login, String url) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
            String plainCreds = "admin:admin";
            byte[] plainCredsBytes = plainCreds.getBytes();
            byte[] base64CredsBytes = Base64.encodeBytesToBytes(plainCredsBytes);
            String base64Creds = new String(base64CredsBytes);

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(new MediaType("application","json"));
            requestHeaders.add("Authorization", "Basic " + base64Creds);

            HttpEntity<Login> requestEntity = new HttpEntity<Login>(login, requestHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<Account> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Account.class);
            Account account = response.getBody();

            return true;
        } catch (Exception e) {
            Log.e("LoginActivity", e.getMessage(), e);
            return false;
        }

    }


}
