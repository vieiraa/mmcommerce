package com.socoletas.mmcommerce.qrcode;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.socoletas.mmcommerce.R;
import com.socoletas.mmcommerce.utils.AssetsPropertyReader;

import java.text.NumberFormat;
import java.util.Properties;

public class QRCodePwActivity extends AppCompatActivity
{

    int WIDTH = 500;
    int HEIGHT = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ericssonbrand);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_pw);
        Switch swt = getSwtBuyerToVendorOnPW();
        swt.setChecked(true);
        swt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                swtBuyerToVendorOnPWChangeListener(compoundButton);
            }
        });
        Button okButton = (Button) findViewById(R.id.OkButton);
        final ImageView imageView = (ImageView) findViewById(R.id.qrCode);
        imageView.setImageResource(0);
        final EditText saleValueText = (EditText) findViewById(R.id.saleValue);

        okButton.setText("OK");
        okButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    String priceValue = saleValueText.getText().toString();
                    String qrcodeFinal = getIntent().getExtras().getString("userName") + ";" + priceValue;
                    Bitmap bitmap = encodeAsBitmap(qrcodeFinal);
                    imageView.setImageBitmap(bitmap);
                } catch (WriterException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private Switch getSwtBuyerToVendorOnPW() {
        return (Switch) findViewById(R.id.swtBuyerToVendorOnPW);
    }

    Bitmap encodeAsBitmap(String str) throws WriterException
    {
        BitMatrix result;
        try
        {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
        } catch (IllegalArgumentException iae)
        {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++)
        {
            int offset = y * w;
            for (int x = 0; x < w; x++)
            {
                pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    }

    public void swtBuyerToVendorOnPW_onClick (View view)
    {
        swtBuyerToVendorOnPWChangeListener(view);
    }

    private void swtBuyerToVendorOnPWChangeListener (View view)
    {
        Switch swt = (Switch)view;
        AssetsPropertyReader assetsPropertyReader = new AssetsPropertyReader(this);
        Properties prp = assetsPropertyReader.getProperties(AssetsPropertyReader.GLOBAL_VALUE_PROPERTIES);
        if (swt.isChecked())
        {
            prp.setProperty(AssetsPropertyReader.WORKING_MODE_KEY, "VENDOR");
        }
        else
        {
            prp.setProperty(AssetsPropertyReader.WORKING_MODE_KEY, "BUYER");
            intentQRCodePRActivity();
        }
    }

    public void intentQRCodePRActivity()
    {
        Intent intent = new Intent(this, QRCodePhotoReader.class);
        Bundle bundle = getIntent().getExtras();
        intent.putExtras(bundle);
        startActivity(intent);
    }

}
