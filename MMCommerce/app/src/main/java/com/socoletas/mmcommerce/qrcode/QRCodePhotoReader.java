package com.socoletas.mmcommerce.qrcode;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.zxing.Result;
import com.socoletas.mmcommerce.AsyncBalanceResponse;
import com.socoletas.mmcommerce.ConfirmationActivity;
import com.socoletas.mmcommerce.Login;
import com.socoletas.mmcommerce.R;
import com.socoletas.mmcommerce.RestAsyncTask;
import com.socoletas.mmcommerce.RestBalanceAsyncTask;
import com.socoletas.mmcommerce.utils.AssetsPropertyReader;
import com.socoletas.mmcommerce.utils.Transanction;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Properties;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodePhotoReader extends AppCompatActivity implements ZXingScannerView.ResultHandler, AsyncBalanceResponse {
    private static final String BALANCE_TEXT = "Balance: R$ ";
    private static final String CEILING_TEXT = "Ceiling: R$ ";
    private ZXingScannerView mScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ericssonbrand);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_photo_reader);
        getUserBalance();
        AssetsPropertyReader assetsPropertyReader = new AssetsPropertyReader(this);
        Properties prp = assetsPropertyReader.getProperties(AssetsPropertyReader.GLOBAL_VALUE_PROPERTIES);
        String val = prp.getProperty(AssetsPropertyReader.WORKING_MODE_KEY);
        Switch swt = getSwitchBuyerToVendor();
        if ("BUYER".equals(val))
        {
            swt.setChecked(false);
        }
        else if ("VENDOR".equals(val))
        {
            swt.setChecked(true);
            intentQRCodePwActivity();
        }
        swt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                swtBuyerToVendorChangeListener(compoundButton);
            }
        });
    }

    private void getUserBalance() {
        Bundle bundle = getIntent().getExtras();
        RestTemplate restTemplate = new RestTemplate();
        Login login = new Login();
        login.setUsername(bundle.getString("userName","Joao"));
        login.setPassword(bundle.getString("password","100"));
        String url = "https://mmcommerce.herokuapp.com/balance";
        RestBalanceAsyncTask restAsyncTask = (RestBalanceAsyncTask) new RestBalanceAsyncTask(login,url,null );
        restAsyncTask.delegate = this;
        restAsyncTask.execute();

    }

    private Switch getSwitchBuyerToVendor() {
        return (Switch) findViewById(R.id.switchBuyerToVendor);
    }

    public void intentQRCodePwActivity()
    {
        Intent intent = new Intent(this, QRCodePwActivity.class);
        Bundle bundle = getIntent().getExtras();
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onStart()
    {
        startQRCodeReader();
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        startQRCodeReader();
        super.onResume();
    }

    public void startQRCodeReader()
    {
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view<br />
        FrameLayout qrCodeFrameLayoutView =(FrameLayout) findViewById(R.id.QRCodeReaderFrameLyout);
        qrCodeFrameLayoutView.addView(mScannerView);
        //setContentView(mScannerView);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.<br />
        mScannerView.startCamera();         // Start camera<br />
    }

    public void swtBuyerToVendor_onClick (View view)
    {
        swtBuyerToVendorChangeListener(view);
    }

    private void swtBuyerToVendorChangeListener (View view)
    {
        Switch swt = (Switch)view;
        AssetsPropertyReader assetsPropertyReader = new AssetsPropertyReader(this);
        Properties prp = assetsPropertyReader.getProperties(AssetsPropertyReader.GLOBAL_VALUE_PROPERTIES);
        if (swt.isChecked())
        {
            prp.setProperty(AssetsPropertyReader.WORKING_MODE_KEY, "VENDOR");
            intentQRCodePwActivity();
        }
        else
        {
            prp.setProperty(AssetsPropertyReader.WORKING_MODE_KEY, "BUYER");
        }
    }

    public void btnConfigurations_onClick (View view)
    {
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here</p>
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Scan result");
        builder.setMessage(rawResult.getText());

        String splited[] = rawResult.getText().split(";");

        Intent intent = new Intent(this, ConfirmationActivity.class);
        Bundle bundle = getIntent().getExtras();
        bundle.putString("vendorName", splited[0]);
        bundle.putString("value", splited[1]);
        intent.putExtras(bundle);
        startActivity(intent);

        // If you would like to resume scanning, call this method below:<br />
        // mScannerView.resumeCameraPreview(this);<br />
    }

    @Override
    public void processFinish(ResponseEntity<String> output) {
        TextView balanceTextView = (TextView) findViewById(R.id.BalanceTextField);
        if (output == null) {
            balanceTextView.setText(BALANCE_TEXT + "1000,00"+" "+CEILING_TEXT + "12000,00");
        }else
        {
            String responseBody = output.getBody();
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(responseBody);
                String balanceVal = jObject.getString("balance");
                String ceilingVal = jObject.getString("ceiling");
                balanceTextView.setText(BALANCE_TEXT + balanceVal + " " +CEILING_TEXT + ceilingVal);
            } catch (JSONException e)
            {
                balanceTextView.setText(BALANCE_TEXT + "1000,00"+" "+CEILING_TEXT + "12000,00");
            }
        }
    }

}
