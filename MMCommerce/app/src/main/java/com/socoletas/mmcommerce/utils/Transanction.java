package com.socoletas.mmcommerce.utils;

/**
 * Created by eaffvie on 8/12/2016.
 */

public class Transanction {
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreditor() {
        return creditor;
    }

    public void setCreditor(String creditor) {
        this.creditor = creditor;
    }

    String password;
    String creditor;
    String amount;
}
