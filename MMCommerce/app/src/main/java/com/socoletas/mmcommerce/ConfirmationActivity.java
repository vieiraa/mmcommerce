package com.socoletas.mmcommerce;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.TransactionTooLargeException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.socoletas.mmcommerce.utils.Transanction;

import org.springframework.web.client.RestTemplate;

public class ConfirmationActivity extends AppCompatActivity implements AsyncResponse {
    private EditText mPasswordView;
    private Bundle bundle;
    private TextView mVendorName;
    private TextView mValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ericssonbrand);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        mPasswordView = (EditText) findViewById(R.id.password);

        Button mOkButton = (Button) findViewById(R.id.OkButton);
        mOkButton.setOnClickListener(view -> confirmTransaction());

        Intent intent = getIntent();
        bundle = intent.getExtras();
        String vendorName = bundle.getString("vendorName");
        String value = bundle.getString("value");
        mVendorName = (TextView) findViewById(R.id.vendorName);
        mVendorName.setText(vendorName);

        mValue = (TextView) findViewById(R.id.valueField);
        mValue.setText(value);
    }

    private void confirmTransaction() {

        String password = mPasswordView.getText().toString();
        RestTemplate restTemplate = new RestTemplate();
        Login login = new Login();
        login.setUsername(bundle.getString("userName","Joao"));
        login.setPassword(bundle.getString("password","100"));

        Transanction transanction = new Transanction();
        mPasswordView = (EditText) findViewById(R.id.password);
        transanction.setPassword(mPasswordView.getText().toString());
        transanction.setAmount(bundle.getString("value"));
        transanction.setCreditor(bundle.getString("vendorName"));
        String url = "https://mmcommerce.herokuapp.com/payment";
//        RestClient restClient = new RestClient();
//        restClient.executePost(login, url);
        RestAsyncTask restAsyncTask = (RestAsyncTask) new RestAsyncTask(login,url,transanction );
        restAsyncTask.delegate = this;
        restAsyncTask.execute();

    }

    @Override
    public void processFinish(Boolean output) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Payment");
        builder.setMessage("Sucsess");
        AlertDialog alert1 = builder.create();
        alert1.show();
    }
}
