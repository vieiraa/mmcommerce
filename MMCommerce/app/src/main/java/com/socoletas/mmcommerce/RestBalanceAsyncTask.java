package com.socoletas.mmcommerce;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.support.Base64;
import org.springframework.web.client.RestTemplate;

/**
 * Created by echamca on 12/7/2016.
 */

public class RestBalanceAsyncTask extends AsyncTask<Void, Void, ResponseEntity<String>> {
    public AsyncBalanceResponse delegate = null;

    private Login mLogin;
    private String mUrl;
    private Object mObject;

    public RestBalanceAsyncTask(Login login, String url, Object object)
    {
        mLogin = login;
        mUrl = url;
        mObject = object;
    }


    @Override
    protected ResponseEntity<String> doInBackground(Void... params) {
        try {

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);

            String authorization = null;
            if(mLogin != null )
            {
                authorization = mLogin.getUsername()+":"+mLogin.getPassword();
            }

            if(authorization != null)
            {
                byte[] plainCredsBytes = authorization.getBytes();
                byte[] base64CredsBytes = Base64.encodeBytesToBytes(plainCredsBytes);
                String base64Creds = new String(base64CredsBytes);
                requestHeaders.add("Authorization", "Basic " + base64Creds);
            }

            HttpEntity<Object> requestEntity = new HttpEntity<Object>(mObject, requestHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            return (ResponseEntity<String>) restTemplate.exchange(mUrl,HttpMethod.GET,requestEntity, String.class);
        } catch (Exception e) {
            Log.e("LoginActivity", e.getMessage(), e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(ResponseEntity<String> result)
    {
        delegate.processFinish(result);

    }

}
