package com.socoletas.mmcommerce;

import org.springframework.http.ResponseEntity;

/**
 * Created by echamca on 12/8/2016.
 */

public interface AsyncBalanceResponse {
    void processFinish(ResponseEntity<String> output);
}
